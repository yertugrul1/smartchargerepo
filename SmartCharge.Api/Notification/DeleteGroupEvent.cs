using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Connector;

namespace SmartCharge.Api.Notification
{
    public class DeleteGroupEvent : INotification
    {
        public DeleteGroupEvent(Guid groupKey)
        {
            GroupKey = groupKey;
        }
        
        public Guid GroupKey { get; }
    }
    
    public class DeleteGroupEventHandler: INotificationHandler<DeleteGroupEvent>
    {
        private readonly IMediator _mediator;

        public DeleteGroupEventHandler(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        public async Task Handle(DeleteGroupEvent deleteGroupEvent, CancellationToken cancellationToken)
        {
            await _mediator.Send(new DeleteConnectorsOfGroup(deleteGroupEvent.GroupKey), cancellationToken);
            await _mediator.Send(new DeleteChargeStationsOfGroup(deleteGroupEvent.GroupKey), cancellationToken);
        }
    }
}