using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SmartCharge.Api.Data.Connector;

namespace SmartCharge.Api.Notification
{
    public class DeleteChargeStationEvent : INotification
    {
        public DeleteChargeStationEvent(Guid stationKey, Guid groupKey)
        {
            StationKey = stationKey;
            GroupKey = groupKey;
        }
        
        public Guid StationKey { get; }
        public Guid GroupKey { get; }
    }
    
    public class DeleteChargeStationEventHandler: INotificationHandler<DeleteChargeStationEvent>
    {
        private readonly IMediator _mediator;

        public DeleteChargeStationEventHandler(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        public async Task Handle(DeleteChargeStationEvent deleteChargeStationEvent, CancellationToken cancellationToken)
        {
            await _mediator.Send(new DeleteConnectorsOfChargeStation(deleteChargeStationEvent.StationKey, deleteChargeStationEvent.GroupKey), cancellationToken);
        }
    }
}