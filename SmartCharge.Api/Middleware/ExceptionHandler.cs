using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Serilog;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Middleware
{
    //Exception Handler in .NetCore Middleware
    public class ExceptionHandler
    {
        private readonly RequestDelegate _next;

        public ExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (CustomException customException)
            {
                Log.Warning(customException, "CustomException");
                await ClearResponseAndBuildErrorDto(context, customException.StatusCode, JsonSerializer.Serialize(new {customException.Message}) ).ConfigureAwait(false);
            }
            catch (ValidationException validationException)
            {
                var messages = new List<string>();
                if (validationException.Errors.Any())
                {
                    messages.AddRange(validationException.Errors.Select(i => i.ErrorMessage));
                }
                else
                {
                    messages.Add(validationException.Message);
                }
                Log.Warning(validationException, "ValidationException");
                await ClearResponseAndBuildErrorDto(context, StatusCodes.Status400BadRequest, validationException.Message).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Log.Error(e, "ServerError");
                await ClearResponseAndBuildErrorDto(context, StatusCodes.Status500InternalServerError,  JsonSerializer.Serialize(new {e.Message}) ).ConfigureAwait(false);
            }
        }

        private static Task ClearResponseAndBuildErrorDto(HttpContext context, int statusCode, string messages)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(messages, Encoding.UTF8);
        }
    }
}