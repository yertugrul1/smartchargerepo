using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Caching.Memory;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Pipeline
{
    //Works directly after any mediatr send request handler is processed in pipeline
    //If any mediatr request inherits from specific interface, lets say IAuditable, it is possible to directly add audit logs or cache invalidation
    // which makes code base more reusable
    public class CacheInvalidationPipelineBehaviour<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IMemoryCache _cache;

        public CacheInvalidationPipelineBehaviour(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
        {
            if (!(request is ICacheInvalidator cacheValidator))
            {
                return;
            }

            foreach (var key in cacheValidator.CacheKeys)
            {
                _cache.Remove(key);
            }
        }
    }
}