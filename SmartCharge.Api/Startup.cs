using System.Linq;
using System.Net.Mime;
using System.Reflection;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SmartCharge.Api.Middleware;
using SmartCharge.Api.Model;
using SmartCharge.Api.Pipeline;

namespace SmartCharge.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "SmartCharge.Api", Version = "v1"});
            });
            
            //Loosely coupled with the power of Mediatr Pipeline structure and dependency injection
            services.AddMediatR(Assembly.GetExecutingAssembly());
            //Cache operation is registered to pipeline
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddSingleton(typeof(IPipelineBehavior<,>), typeof(CachePipelineBehaviour<,>));
            
            //Register IValidators to IOC Container
            foreach (var implementationType in typeof(Startup)
                .Assembly
                .ExportedTypes
                .Where(t => t.IsClass && !t.IsAbstract))
            {
                foreach (var serviceType in implementationType
                    .GetInterfaces()
                    .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IValidator<>)))
                {
                    services.Add(new ServiceDescriptor(serviceType, implementationType, ServiceLifetime.Transient));
                }
            }
            services.AddMemoryCache();
            
            //Inject datatabase connection string
            var conn = Configuration.GetSection("ConnectionString").Get<ConnectionString>().Database;
            services.AddSingleton(new ConnectionString { Database = conn});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SmartCharge.Api v1"));
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            //Middleware pipeline register, you can add more tasks to middleware, like logger etc
            app.UseMiddleware<ExceptionHandler>();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}