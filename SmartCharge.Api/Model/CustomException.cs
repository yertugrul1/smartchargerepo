using System;
using System.Net;

namespace SmartCharge.Api.Model
{
    public class CustomException : Exception
    {
        public CustomException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = (int)statusCode;
        }
        public int StatusCode { get; }
    }
}