using System;

namespace SmartCharge.Api.Model
{
    public interface ICacheable
    {
        string CacheKey { get; }
        TimeSpan CacheDuration { get; }
    }
}