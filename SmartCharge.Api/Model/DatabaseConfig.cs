namespace SmartCharge.Api.Model
{
    public class ConnectionString
    {
        public string Database { get; set; }
    }
}