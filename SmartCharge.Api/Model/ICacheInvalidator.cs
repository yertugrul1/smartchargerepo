using System.Collections.Generic;

namespace SmartCharge.Api.Model
{
    public interface ICacheInvalidator
    {
        List<string> CacheKeys { get; }
    }
}