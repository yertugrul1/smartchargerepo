using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SmartCharge.Api.Service.ChargeStation;

namespace SmartCharge.Api.Controllers
{
    [Route("v1/charge-station")]
    [ApiController]
    public class ChargeStationController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public ChargeStationController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Add Charge Station
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> AddChargeStation([FromBody] AddChargeStation station)
        {
            await _mediator.Send(station);
            return NoContent();
        }
        
        /// <summary>
        /// Update Charge Station
        /// </summary>
        [HttpPut]
        public async Task<ActionResult> UpdateChargeStation([FromBody] UpdateChargeStation station)
        {
            await _mediator.Send(station);
            return NoContent();
        }
        
        /// <summary>
        /// Delete Charge Station
        /// </summary>
        [HttpDelete("{key}")]
        public async Task<ActionResult> DeleteChargeStation([FromRoute] Guid key)
        {
            await _mediator.Send(new DeleteChargeStation(key));
            return NoContent();
        }
    }
}