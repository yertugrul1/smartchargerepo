using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SmartCharge.Api.Service.Connector;

namespace SmartCharge.Api.Controllers
{
    [Route("v1/connector")]
    [ApiController]
    public class ConnectorController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public ConnectorController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Add Connector
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> AddConnector([FromBody] AddConnector connector)
        {
            await _mediator.Send(connector);
            return NoContent();
        }
        
        /// <summary>
        /// Update Connector
        /// </summary>
        [HttpPut]
        public async Task<ActionResult> UpdateConnector([FromBody] UpdateConnector connector)
        {
            await _mediator.Send(connector);
            return NoContent();
        }
        
        /// <summary>
        /// Delete Connector
        /// </summary>
        [HttpDelete("{stationKey}/{id}")]
        public async Task<ActionResult> DeleteConnector([FromRoute] Guid stationKey, int id)
        {
            await _mediator.Send(new DeleteConnector(stationKey, id));
            return NoContent();
        }
    }
}