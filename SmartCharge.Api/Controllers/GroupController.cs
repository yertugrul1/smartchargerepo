using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SmartCharge.Api.Service.Group;

namespace SmartCharge.Api.Controllers
{
    [Route("v1/group")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public GroupController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Add Group
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> AddGroup([FromBody] AddGroup request)
        {
            await _mediator.Send(request);
            return NoContent();
        }
        
        /// <summary>
        /// Update Group
        /// </summary>
        [HttpPut]
        public async Task<ActionResult> UpdateGroup([FromBody] UpdateGroup request)
        {
            await _mediator.Send(request);
            return NoContent();
        }
        
        /// <summary>
        /// Delete Group
        /// </summary>
        [HttpDelete("{key}")]
        public async Task<ActionResult> DeleteGroup([FromRoute] Guid key)
        {
            await _mediator.Send(new DeleteGroup(key));
            return NoContent();
        }
    }
}