using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.Connector;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.Group
{
    public class UpdateGroup : IRequest
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
        public double Capacity { get; set; }
    }
    
    public class UpdateGroupValidator : AbstractValidator<UpdateGroup>
    {
        public UpdateGroupValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
            RuleFor(d => d.Name).NotEmpty().WithMessage("Name cannot be empty!");
            RuleFor(d => d.Capacity).NotEmpty().GreaterThan(0).WithMessage("Capacity must be greater than 0!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class UpdateGroupHandler : IRequestHandler<UpdateGroup>
    {
        private readonly IMediator _mediator;

        public UpdateGroupHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateGroup request, CancellationToken cancellationToken)
        {
            var totalCurrent = await _mediator.Send(new GetMaxCurrentOfGroup(request.Key), cancellationToken);
            if (totalCurrent > request.Capacity)
            {
                throw new CustomException("Max current is larger than requested capacity!", HttpStatusCode.BadRequest);
            }
            
            var affectedRows = await _mediator.Send(new Data.Group.UpdateGroup(request.Name, request.Key, request.Capacity), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The group is not found!", HttpStatusCode.NotFound);
            }
            
            return Unit.Value;
        }
    }
}