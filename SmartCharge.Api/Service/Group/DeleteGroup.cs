using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using SmartCharge.Api.Notification;

namespace SmartCharge.Api.Service.Group
{
    public class DeleteGroup : IRequest
    {
        public DeleteGroup(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
    }
    
    public class DeleteGroupValidator : AbstractValidator<DeleteGroup>
    {
        public DeleteGroupValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class DeleteGroupHandler : IRequestHandler<DeleteGroup>
    {
        private readonly IMediator _mediator;

        public DeleteGroupHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteGroup request, CancellationToken cancellationToken)
        {
            var group = await _mediator.Send(new GetGroup(request.Key), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The group is not found!", HttpStatusCode.NotFound);
            }
            
            // Decouple extra commands by publishing event to notifiers
            await _mediator.Publish(new DeleteGroupEvent(request.Key), cancellationToken);
            await _mediator.Send(new Data.Group.DeleteGroup(request.Key), cancellationToken);
            
            return Unit.Value;
        }
    }
}