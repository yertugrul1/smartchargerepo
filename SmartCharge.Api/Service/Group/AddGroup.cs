using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.Group
{
    public class AddGroup : IRequest
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
        public double Capacity { get; set; }
    }
    
    public class AddGroupValidator : AbstractValidator<AddGroup>
    {
        public AddGroupValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
            RuleFor(d => d.Name).NotEmpty().WithMessage("Name cannot be empty!");
            RuleFor(d => d.Capacity).NotEmpty().GreaterThan(0).WithMessage("Capacity must be greater than 0!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class AddGroupHandler : IRequestHandler<AddGroup>
    {
        private readonly IMediator _mediator;

        public AddGroupHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AddGroup request, CancellationToken cancellationToken)
        {
            var affectedRows = await _mediator.Send(new Data.Group.AddGroup(request.Name, request.Key, request.Capacity), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The group already exists!", HttpStatusCode.BadRequest);
            }
            
            return Unit.Value;
        }
    }
}