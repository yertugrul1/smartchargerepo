using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.ChargeStation
{
    public class UpdateChargeStation : IRequest
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
    }
    
    public class UpdateChargeStationValidator : AbstractValidator<UpdateChargeStation>
    {
        public UpdateChargeStationValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
            RuleFor(d => d.Name).NotEmpty().WithMessage("Name cannot be empty!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class UpdateChargeStationHandler : IRequestHandler<UpdateChargeStation>
    {
        private readonly IMediator _mediator;

        public UpdateChargeStationHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateChargeStation request, CancellationToken cancellationToken)
        {
            var affectedRows = await _mediator.Send(new Data.ChargeStation.UpdateChargeStation(request.Name, request.Key), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The charge station is not found!", HttpStatusCode.NotFound);
            }
            
            return Unit.Value;
        }
    }
}