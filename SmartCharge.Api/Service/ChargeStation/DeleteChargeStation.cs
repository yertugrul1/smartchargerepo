using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using SmartCharge.Api.Notification;

namespace SmartCharge.Api.Service.ChargeStation
{
    public class DeleteChargeStation : IRequest
    {
        public DeleteChargeStation(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
    }
    
    public class DeleteChargeStationValidator : AbstractValidator<DeleteChargeStation>
    {
        public DeleteChargeStationValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class DeleteChargeStationHandler : IRequestHandler<DeleteChargeStation>
    {
        private readonly IMediator _mediator;

        public DeleteChargeStationHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteChargeStation request, CancellationToken cancellationToken)
        {
            var station = await _mediator.Send(new GetStation(request.Key), cancellationToken);
            if (station == null)
            {
                throw new CustomException("The charge station is not found!", HttpStatusCode.NotFound);
            }

            var group = await _mediator.Send(new GetGroup(new Guid(station.GroupKey)), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The charge station does not have any group!", HttpStatusCode.BadRequest);
            }
            
            await _mediator.Send(new Data.ChargeStation.DeleteChargeStation(request.Key), cancellationToken);

            // Decouple extra commands by publishing event to notifiers
            var groupKey = new Guid(group.Key);
            await _mediator.Publish(new DeleteChargeStationEvent(request.Key, groupKey), cancellationToken);
            
            return Unit.Value;
        }
    }
}