using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.ChargeStation
{
    public class AddChargeStation : IRequest
    {
        public Guid Key { get; set; }
        public string Name { get; set; }
        public Guid GroupKey { get; set; }
    }
    
    public class AddChargeStationValidator : AbstractValidator<AddChargeStation>
    {
        public AddChargeStationValidator()
        {
            RuleFor(d => d.Key).NotEmpty().WithMessage("Key cannot be empty!");
            RuleFor(d => d.Key).SetValidator(new GuidValidator()).WithMessage("Key must be Guid!");
            RuleFor(d => d.Name).NotEmpty().WithMessage("Name cannot be empty!");
            RuleFor(d => d.GroupKey).NotEmpty().WithMessage("GroupKey cannot be empty!");
            RuleFor(d => d.GroupKey).SetValidator(new GuidValidator()).WithMessage("GroupKey must be Guid!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class AddChargeStationHandler : IRequestHandler<AddChargeStation>
    {
        private readonly IMediator _mediator;

        public AddChargeStationHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AddChargeStation request, CancellationToken cancellationToken)
        {
            var group = await _mediator.Send(new GetGroup(request.GroupKey), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The group is not found!", HttpStatusCode.NotFound);
            }
            
            var affectedRows = await _mediator.Send(new Data.ChargeStation.AddChargeStation(request.Name, request.Key, request.GroupKey), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The charge station already exists!", HttpStatusCode.BadRequest);
            }
            
            return Unit.Value;
        }
    }
}