using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Connector;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.Connector
{
    public class AddConnector : IRequest
    {
        public int Id { get; set; }
        public Guid StationKey { get; set; }
        public double MaxCurrent { get; set; }
    }
    
    public class AddConnectorValidator : AbstractValidator<AddConnector>
    {
        public AddConnectorValidator()
        {
            RuleFor(d => d.Id).NotEmpty().GreaterThan(0).LessThanOrEqualTo(5).WithMessage("Id must be between 0 and 5!");
            RuleFor(d => d.StationKey).SetValidator(new GuidValidator()).WithMessage("StationKey must be Guid!");
            RuleFor(d => d.StationKey).NotEmpty().WithMessage("StationKey cannot be empty!");
            RuleFor(d => d.MaxCurrent).NotEmpty().GreaterThan(0).WithMessage("MaxCurrent must be greater than 0!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class AddConnectorHandler : IRequestHandler<AddConnector>
    {
        private readonly IMediator _mediator;

        public AddConnectorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AddConnector request, CancellationToken cancellationToken)
        {
            var station = await _mediator.Send(new GetStation(request.StationKey), cancellationToken);
            if (station == null)
            {
                throw new CustomException("The charge station does not exists!", HttpStatusCode.BadRequest);
            }

            var group = await _mediator.Send(new GetGroup(new Guid(station.GroupKey)), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The charge station does not have any group!", HttpStatusCode.BadRequest);
            }
            
            var groupKey = new Guid(group.Key);
            var totalCurrent = await _mediator.Send(new GetMaxCurrentOfGroup(groupKey), cancellationToken);
            if (group.Capacity < totalCurrent + request.MaxCurrent)
            {
                throw new CustomException("The group capacity is exceeding!", HttpStatusCode.BadRequest);
            }
            
            var affectedRows = await _mediator.Send(new Data.Connector.AddConnector(groupKey, request.Id, request.StationKey, request.MaxCurrent), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The connector already exists!", HttpStatusCode.BadRequest);
            }
            
            return Unit.Value;
        }
    }
}