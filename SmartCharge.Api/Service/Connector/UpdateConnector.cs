using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Connector;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.Connector
{
    public class UpdateConnector : IRequest
    {
        public int Id { get; set; }
        public double MaxCurrent { get; set; }
        public Guid StationKey { get; set; }
    }
    
    public class UpdateConnectorValidator : AbstractValidator<UpdateConnector>
    {
        public UpdateConnectorValidator()
        {
            RuleFor(d => d.Id).NotEmpty().GreaterThan(0).LessThanOrEqualTo(5).WithMessage("Id must be between 0 and 5!");
            RuleFor(d => d.MaxCurrent).NotEmpty().GreaterThan(0).WithMessage("MaxCurrent must be greater than 0!");
            RuleFor(d => d.StationKey).SetValidator(new GuidValidator()).WithMessage("StationKey must be Guid!");
            RuleFor(d => d.StationKey).NotEmpty().WithMessage("StationKey cannot be empty!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class UpdateConnectorHandler : IRequestHandler<UpdateConnector>
    {
        private readonly IMediator _mediator;

        public UpdateConnectorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateConnector request, CancellationToken cancellationToken)
        {
            var station = await _mediator.Send(new GetStation(request.StationKey), cancellationToken);
            if (station == null)
            {
                throw new CustomException("The charge station does not exists!", HttpStatusCode.BadRequest);
            }

            var group = await _mediator.Send(new GetGroup(new Guid(station.GroupKey)), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The charge station does not have any group!", HttpStatusCode.BadRequest);
            }
            
            var groupKey = new Guid(group.Key);
            var updatedConnector = await _mediator.Send(new GetConnector(request.StationKey, request.Id), cancellationToken);
            if (updatedConnector == null)
            {
                throw new CustomException("The connector is not found!", HttpStatusCode.NotFound);
            }
            
            var totalCurrent = await _mediator.Send(new GetMaxCurrentOfGroup(groupKey), cancellationToken);
            if (group.Capacity < totalCurrent + request.MaxCurrent - updatedConnector.MaxCurrent)
            {
                throw new CustomException("The group capacity is exceeding!", HttpStatusCode.BadRequest);
            }
            
            await _mediator.Send(new Data.Connector.UpdateConnector(groupKey, request.StationKey, request.Id, request.MaxCurrent), cancellationToken);

            return Unit.Value;
        }
    }
}