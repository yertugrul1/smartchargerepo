using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Service.Connector
{
    public class DeleteConnector : IRequest
    {
        public DeleteConnector(Guid station_key, int id)
        {
            StationKey = station_key;
            Id = id;
        }
        public Guid StationKey { get; }
        public int Id { get; }
    }
    
    public class DeleteConnectorValidator : AbstractValidator<DeleteConnector>
    {
        public DeleteConnectorValidator()
        {
            RuleFor(d => d.Id).NotEmpty().GreaterThan(0).LessThanOrEqualTo(5).WithMessage("Id must be between 0 and 5!");
            RuleFor(d => d.StationKey).SetValidator(new GuidValidator()).WithMessage("StationKey must be Guid!");
            RuleFor(d => d.StationKey).NotEmpty().WithMessage("StationKey cannot be empty!");
        }
        
        private class GuidValidator : AbstractValidator<Guid>
        {
            public GuidValidator()
            {
                RuleFor(x => x).NotNull().NotEmpty();
            }
        }
    }

    public class DeleteConnectorHandler : IRequestHandler<DeleteConnector>
    {
        private readonly IMediator _mediator;

        public DeleteConnectorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteConnector request, CancellationToken cancellationToken)
        {
            var station = await _mediator.Send(new GetStation(request.StationKey), cancellationToken);
            if (station == null)
            {
                throw new CustomException("The charge station does not exists!", HttpStatusCode.BadRequest);
            }

            var group = await _mediator.Send(new GetGroup(new Guid(station.GroupKey)), cancellationToken);
            if (group == null)
            {
                throw new CustomException("The charge station does not have any group!", HttpStatusCode.BadRequest);
            }
            
            var groupKey = new Guid(group.Key);
            var affectedRows = await _mediator.Send(new Data.Connector.DeleteConnector(groupKey, request.StationKey, request.Id), cancellationToken);
            if (affectedRows == 0)
            {
                throw new CustomException("The connector is not found!", HttpStatusCode.NotFound);
            }
            
            return Unit.Value;
        }
    }
}