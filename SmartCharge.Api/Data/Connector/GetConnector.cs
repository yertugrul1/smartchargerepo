using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class GetConnector : IRequest<GetConnectorResponse>, ICacheable
    {
        public GetConnector(Guid stationKey, int id)
        {
            StationKey = stationKey;
            Id = id;
        }
        public Guid StationKey { get; }
        public int Id { get; }
        public string CacheKey => $"connector:{StationKey}:{Id}";
        public TimeSpan CacheDuration => TimeSpan.FromHours(1);
    }

    public class GetConnectorResponse
    {
        public int Id { get; set; }
        public double MaxCurrent { get; set; }
    }

    public class GetConnectorHandler : IRequestHandler<GetConnector, GetConnectorResponse>
    {
        private readonly ConnectionString _connectionString;

        public GetConnectorHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<GetConnectorResponse> Handle(GetConnector request, CancellationToken cancellationToken)
        {
            const string query = @"SELECT id As Id,
                                          max_current AS MaxCurrent
                                   FROM connectors
                                   WHERE station_key = @StationKey AND id = @Id";
            
            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.QuerySingleOrDefaultAsync<GetConnectorResponse>(query, request);
            }
        }
    }
}