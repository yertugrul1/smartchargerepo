using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class DeleteConnector : IRequest<int>, ICacheInvalidator
    {
        public DeleteConnector(Guid groupKey, Guid stationKey, int id)
        {
            StationKey = stationKey;
            CacheKeys = new List<string>{$"{groupKey}:totalcurrent", $"connector:{StationKey}:{Id}"};
            Id = id;
        }
        public int Id { get; }
        public Guid StationKey { get; }
        public List<string> CacheKeys { get; }
    }

    public class DeleteConnectorHandler : IRequestHandler<DeleteConnector, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteConnectorHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteConnector request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM connectors WHERE station_key = @StationKey AND id = @Id";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}