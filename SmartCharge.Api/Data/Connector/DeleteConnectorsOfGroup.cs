using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class DeleteConnectorsOfGroup : IRequest<int>, ICacheInvalidator
    {
        public DeleteConnectorsOfGroup(Guid groupKey)
        {
            GroupKey = groupKey;
        }
        public Guid GroupKey { get; }
        public List<string> CacheKeys => new List<string>{$"{GroupKey}:totalcurrent"};
    }

    public class DeleteConnectorsOfGroupHandler : IRequestHandler<DeleteConnectorsOfGroup, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteConnectorsOfGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteConnectorsOfGroup request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM connectors 
                                   WHERE station_key IN(SELECT cs.key FROM groups g
                                                        INNER JOIN charge_stations cs ON g.key = cs.group_key
                                                        WHERE g.key = @GroupKey)";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}