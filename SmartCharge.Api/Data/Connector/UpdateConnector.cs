using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class UpdateConnector : IRequest<int>, ICacheInvalidator
    {
        public UpdateConnector(Guid groupKey, Guid stationKey, int id, double maxCurrent)
        {
            MaxCurrent = maxCurrent;
            Id = id;
            StationKey = stationKey;
            CacheKeys = new List<string>{$"{groupKey}:totalcurrent", $"connector:{StationKey}:{Id}"};
        }
        
        public Guid StationKey { get; }
        public double MaxCurrent { get; }
        public int Id { get; }
        public List<string> CacheKeys { get; }
    }

    public class UpdateConnectorHandler : IRequestHandler<UpdateConnector, int>
    {
        private readonly ConnectionString _connectionString;

        public UpdateConnectorHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(UpdateConnector request, CancellationToken cancellationToken)
        {
            const string query = @"UPDATE connectors SET max_current = @MaxCurrent
                                   WHERE station_key = @StationKey AND id = @Id";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}