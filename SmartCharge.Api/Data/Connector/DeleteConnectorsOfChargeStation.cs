using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class DeleteConnectorsOfChargeStation : IRequest<int>, ICacheInvalidator
    {
        public DeleteConnectorsOfChargeStation(Guid stationKey, Guid groupKey)
        {
            StationKey = stationKey;
            CacheKeys = new List<string>{$"{groupKey}:totalcurrent"};
        }
        public Guid StationKey { get; }
        public List<string> CacheKeys { get; }
    }

    public class DeleteConnectorsOfChargeStationHandler : IRequestHandler<DeleteConnectorsOfChargeStation, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteConnectorsOfChargeStationHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteConnectorsOfChargeStation request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM connectors 
                                   WHERE station_key = @StationKey";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}