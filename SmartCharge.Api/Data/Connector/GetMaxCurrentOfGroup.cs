using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class GetMaxCurrentOfGroup : IRequest<double>, ICacheable
    {
        public GetMaxCurrentOfGroup(Guid groupKey)
        {
            GroupKey = groupKey;
        }
        public Guid GroupKey { get; }
        public string CacheKey => $"{GroupKey}:totalcurrent";
        public TimeSpan CacheDuration => TimeSpan.FromHours(1);
    }
    
    public class GetMaxCurrentOfGroupHandler : IRequestHandler<GetMaxCurrentOfGroup, double>
    {
        private readonly ConnectionString _connectionString;

        public GetMaxCurrentOfGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<double> Handle(GetMaxCurrentOfGroup request, CancellationToken cancellationToken)
        {
            const string query = @"SELECT SUM(max_current)
                                   FROM groups g
                                   INNER JOIN charge_stations cs ON g.key = cs.group_key
                                   INNER JOIN connectors c ON cs.key = c.station_key
                                   WHERE g.key = @GroupKey";
            
            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteScalarAsync<double>(query, request);
            }
        }
    }
}