using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Connector
{
    public class AddConnector : IRequest<int>, ICacheInvalidator
    {
        public AddConnector(Guid groupKey, int id, Guid stationKey, double maxCurrent)
        {
            Id = id;
            StationKey = stationKey;
            MaxCurrent = maxCurrent;
            CacheKeys = new List<string>{$"{groupKey}:totalcurrent", $"connector:{StationKey}:{Id}"};
        }
        public int Id { get; }
        public Guid StationKey { get; }
        public double MaxCurrent { get; }
        public List<string> CacheKeys { get; }
    }

    public class AddConnectorHandler : IRequestHandler<AddConnector, int>
    {
        private readonly ConnectionString _connectionString;

        public AddConnectorHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(AddConnector request, CancellationToken cancellationToken)
        {
            const string query = @"INSERT INTO connectors(id, station_key, max_current, create_date_utc)
                                   SELECT @Id, @StationKey, @MaxCurrent, DATETIME('now')
                                   WHERE NOT EXISTS (SELECT 1 FROM connectors WHERE station_key = @StationKey AND id = @Id)";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}