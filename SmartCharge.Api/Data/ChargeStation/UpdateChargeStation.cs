using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.ChargeStation
{
    public class UpdateChargeStation : IRequest<int>, ICacheInvalidator
    {
        public UpdateChargeStation(string name, Guid key)
        {
            Name = name;
            Key = key;
        }
        public string Name { get; }
        public Guid Key { get; }
        public List<string> CacheKeys => new List<string>{$"station:{Key}"};
    }

    public class UpdateChargeStationHandler : IRequestHandler<UpdateChargeStation, int>
    {
        private readonly ConnectionString _connectionString;

        public UpdateChargeStationHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(UpdateChargeStation request, CancellationToken cancellationToken)
        {
            const string query = @"UPDATE charge_stations SET name = @Name
                                   WHERE key = @Key";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}