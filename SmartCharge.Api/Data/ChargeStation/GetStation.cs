using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.ChargeStation
{
    public class GetStation : IRequest<GetStationResponse>, ICacheable
    {
        public GetStation(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
        public string CacheKey => $"station:{Key}";
        public TimeSpan CacheDuration => TimeSpan.FromHours(1);
    }

    public class GetStationResponse
    {
        public string StationKey { get; set; }
        public string GroupKey { get; set; }
        public string Name { get; set; }
    }

    public class GetStationHandler : IRequestHandler<GetStation, GetStationResponse>
    {
        private readonly ConnectionString _connectionString;

        public GetStationHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<GetStationResponse> Handle(GetStation request, CancellationToken cancellationToken)
        {
            const string query = @"SELECT key As Key,
                                          name AS Name,
                                          group_key AS GroupKey
                                   FROM charge_stations
                                   WHERE key = @Key";
            
            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.QuerySingleOrDefaultAsync<GetStationResponse>(query, request);
            }
        }
    }
}