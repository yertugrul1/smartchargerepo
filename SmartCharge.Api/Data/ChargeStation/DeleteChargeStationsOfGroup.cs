using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.ChargeStation
{
    public class DeleteChargeStationsOfGroup : IRequest<int>
    {
        public DeleteChargeStationsOfGroup(Guid groupKey)
        {
            GroupKey = groupKey;
        }
        public Guid GroupKey { get; }
    }

    public class DeleteChargeStationsOfGroupHandler : IRequestHandler<DeleteChargeStationsOfGroup, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteChargeStationsOfGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteChargeStationsOfGroup request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM charge_stations WHERE group_key = @GroupKey";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}