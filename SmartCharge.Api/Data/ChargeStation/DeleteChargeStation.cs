using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.ChargeStation
{
    public class DeleteChargeStation : IRequest<int>, ICacheInvalidator
    {
        public DeleteChargeStation(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
        public List<string> CacheKeys => new List<string>{$"station:{Key}"};
    }

    public class DeleteChargeStationHandler : IRequestHandler<DeleteChargeStation, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteChargeStationHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteChargeStation request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM charge_stations WHERE key = @Key";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}