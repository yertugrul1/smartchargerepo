using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.ChargeStation
{
    public class AddChargeStation : IRequest<int>
    {
        public AddChargeStation(string name, Guid key, Guid groupKey)
        {
            Name = name;
            Key = key;
            GroupKey = groupKey;
        }
        public string Name { get; }
        public Guid Key { get; }
        public Guid GroupKey { get; }
    }

    public class AddChargeStationHandler : IRequestHandler<AddChargeStation, int>
    {
        private readonly ConnectionString _connectionString;

        public AddChargeStationHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(AddChargeStation request, CancellationToken cancellationToken)
        {
            const string query = @"INSERT INTO charge_stations(key, name, group_key, create_date_utc) 
                                   SELECT @Key, @Name, @GroupKey, DATETIME('now')
                                   WHERE NOT EXISTS (SELECT 1 FROM charge_stations
                                   WHERE key = @Key)";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}