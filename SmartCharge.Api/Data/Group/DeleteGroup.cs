using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Group
{
    public class DeleteGroup : IRequest<int>, ICacheInvalidator
    {
        public DeleteGroup(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
        public List<string> CacheKeys => new List<string>{$"group:{Key}"};
    }

    public class DeleteGroupHandler : IRequestHandler<DeleteGroup, int>
    {
        private readonly ConnectionString _connectionString;

        public DeleteGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(DeleteGroup request, CancellationToken cancellationToken)
        {
            const string query = @"DELETE FROM groups WHERE key = @Key";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}