using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Group
{
    public class GetGroup : IRequest<GetGroupResponse>, ICacheable
    {
        public GetGroup(Guid key)
        {
            Key = key;
        }
        public Guid Key { get; }
        public string CacheKey => $"group:{Key}";
        public TimeSpan CacheDuration => TimeSpan.FromHours(1);
    }

    public class GetGroupResponse
    {
        public string Key { get; set; }
        public double Capacity { get; set; }
    }

    public class GetGroupHandler : IRequestHandler<GetGroup, GetGroupResponse>
    {
        private readonly ConnectionString _connectionString;

        public GetGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<GetGroupResponse> Handle(GetGroup request, CancellationToken cancellationToken)
        {
            const string query = @"SELECT key As Key,
                                          capacity AS Capacity
                                   FROM groups
                                   WHERE key = @Key";
            
            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.QuerySingleOrDefaultAsync<GetGroupResponse>(query, request);
            }
        }
    }
}