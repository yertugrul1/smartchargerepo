using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Group
{
    public class UpdateGroup : IRequest<int>, ICacheInvalidator
    {
        public UpdateGroup(string name, Guid key, double capacity)
        {
            Name = name;
            Key = key;
            Capacity = capacity;
        }
        public string Name { get; }
        public Guid Key { get; }
        public double Capacity { get; }
        public List<string> CacheKeys => new List<string>{$"group:{Key}"};
    }

    public class UpdateGroupHandler : IRequestHandler<UpdateGroup, int>
    {
        private readonly ConnectionString _connectionString;

        public UpdateGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(UpdateGroup request, CancellationToken cancellationToken)
        {
            const string query = @"UPDATE groups SET name = @Name, capacity = @Capacity
                                   WHERE key = @Key";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}