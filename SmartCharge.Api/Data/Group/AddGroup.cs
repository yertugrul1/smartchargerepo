using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MediatR;
using Microsoft.Data.Sqlite;
using SmartCharge.Api.Model;

namespace SmartCharge.Api.Data.Group
{
    public class AddGroup : IRequest<int>
    {
        public AddGroup(string name, Guid key, double capacity)
        {
            Name = name;
            Key = key;
            Capacity = capacity;
        }
        public string Name { get; }
        public Guid Key { get; }
        public double Capacity { get; }
    }

    public class AddGroupHandler : IRequestHandler<AddGroup, int>
    {
        private readonly ConnectionString _connectionString;

        public AddGroupHandler(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Handle(AddGroup request, CancellationToken cancellationToken)
        {
            const string query = @"INSERT INTO groups(key, name, capacity, create_date_utc) 
                                   SELECT @Key, @Name, @Capacity, DATETIME('now')
                                   WHERE NOT EXISTS (SELECT 1 FROM groups
                                   WHERE key = @Key)";

            using (var connection = new SqliteConnection(_connectionString.Database))
            {
                return await connection.ExecuteAsync(query, request);
            }
        }
    }
}