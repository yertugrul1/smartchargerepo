// using System;
// using System.Threading;
// using System.Threading.Tasks;
// using Dapper;
// using MediatR;
// using Microsoft.Data.Sqlite;
// using SmartCharge.Api.Model;
//
// namespace SmartCharge.Api.Data.Group
// {
//     public class GetGroupByStation : IRequest<GetGroupByStationResponse>, ICacheable
//     {
//         public GetGroupByStation(Guid key)
//         {
//             Key = key;
//         }
//         public Guid Key { get; }
//         public string CacheKey => $"{Key}:group";
//         public TimeSpan CacheDuration => TimeSpan.FromHours(1);
//     }
//
//     public class GetGroupByStationResponse
//     {
//         public string Key { get; set; }
//         public double Capacity { get; set; }
//     }
//
//     public class GetGroupByStationHandler : IRequestHandler<GetGroupByStation, GetGroupByStationResponse>
//     {
//         private readonly ConnectionString _connectionString;
//
//         public GetGroupByStationHandler(ConnectionString connectionString)
//         {
//             _connectionString = connectionString;
//         }
//
//         public async Task<GetGroupByStationResponse> Handle(GetGroupByStation request, CancellationToken cancellationToken)
//         {
//             const string query = @"SELECT g.key As Key,
//                                           capacity AS Capacity
//                                    FROM groups g
//                                    WHERE key = (SELECT group_key FROM charge_stations WHERE key = @Key)";
//             
//             using (var connection = new SqliteConnection(_connectionString.Database))
//             {
//                 return await connection.QuerySingleOrDefaultAsync<GetGroupByStationResponse>(query, request);
//             }
//         }
//     }
// }