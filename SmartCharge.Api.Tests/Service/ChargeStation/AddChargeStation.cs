using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using SmartCharge.Api.Service.ChargeStation;
using Xunit;

namespace SmartCharge.Api.Tests.Service.ChargeStation
{
    public class AddChargeStation
    {
        public class AddChargeStationFixture
        {
            private readonly Mock<IMediator> _mockMediator;

            public AddChargeStationFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }

            public AddChargeStationFixture SetupMediator(GetGroupResponse group, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(group);
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.ChargeStation.AddChargeStation>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(affectedRows);
                return this;
            }

            public AddChargeStationFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }

            public AddChargeStationFixture CheckAddChargeStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.ChargeStation.AddChargeStation>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }

            public AddChargeStationHandler CreateSut()
            {
                return new AddChargeStationHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectNotFound_WhenGroupNotFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.AddChargeStation();
            var fixture = new AddChargeStationFixture();
            var addChargeStationHandler = fixture
                .SetupMediator(null, 0)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await addChargeStationHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckAddChargeStation(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group is not found!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenChargeStationDoesNotExist()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.AddChargeStation();
            var fixture = new AddChargeStationFixture();
            var addChargeStationHandler = fixture
                .SetupMediator(new GetGroupResponse(), 0)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await addChargeStationHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckAddChargeStation(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station already exists!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ExpectDeleted_WhenGroupFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.AddChargeStation();
            var fixture = new AddChargeStationFixture();
            var addChargeStationFixture = fixture
                .SetupMediator(new GetGroupResponse(), 1)
                .CreateSut();

            //Act
            await addChargeStationFixture.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckAddChargeStation(Times.Once());
        }
    }
}