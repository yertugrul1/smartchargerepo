using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using SmartCharge.Api.Notification;
using Xunit;
using DeleteChargeStationHandler = SmartCharge.Api.Service.ChargeStation.DeleteChargeStationHandler;

namespace SmartCharge.Api.Tests.Service.ChargeStation
{
    public class DeleteChargeStation
    {
        public class DeleteChargeStationFixture
        {
            private readonly Mock<IMediator> _mockMediator;

            public DeleteChargeStationFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }

            public DeleteChargeStationFixture SetupMediator(GetGroupResponse group, GetStationResponse station, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(station);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(group);
                return this;
            }

            public DeleteChargeStationFixture CheckDeleteChargeStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.ChargeStation.DeleteChargeStation>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }
            
            public DeleteChargeStationFixture CheckGetStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }
            
            public DeleteChargeStationFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }
            
            public DeleteChargeStationFixture CheckDeleteChargeStationEvent(Times times)
            {
                _mockMediator.Verify(o => o.Publish(It.IsAny<DeleteChargeStationEvent>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }

            public DeleteChargeStationHandler CreateSut()
            {
                return new DeleteChargeStationHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectNotFound_WhenStationNotFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.DeleteChargeStation(Guid.Empty);
            var fixture = new DeleteChargeStationFixture();
            var deleteChargeStationHandler = fixture
                .SetupMediator(null, null, 0)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await deleteChargeStationHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckDeleteChargeStation(Times.Never());
            fixture.CheckGetGroup(Times.Never());
            fixture.CheckGetStation(Times.Once());
            fixture.CheckDeleteChargeStationEvent(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station is not found!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenStationHasNoGroup()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.DeleteChargeStation(Guid.Empty);
            var fixture = new DeleteChargeStationFixture();
            var deleteChargeStationHandler = fixture
                .SetupMediator(null, new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, 0)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await deleteChargeStationHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckDeleteChargeStation(Times.Never());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetStation(Times.Once());
            fixture.CheckDeleteChargeStationEvent(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not have any group!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ExpectDeleted_WhenStationAndGroupFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.DeleteChargeStation(Guid.Empty);
            var fixture = new DeleteChargeStationFixture();
            var deleteChargeStationHandler = fixture
                .SetupMediator(new GetGroupResponse{Key = Guid.NewGuid().ToString()}, new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, 1)
                .CreateSut();

            //Act
            await deleteChargeStationHandler.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckDeleteChargeStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetStation(Times.Once());
            fixture.CheckDeleteChargeStationEvent(Times.Once());
        }
    }
}