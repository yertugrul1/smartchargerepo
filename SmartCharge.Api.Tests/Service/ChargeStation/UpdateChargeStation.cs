using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Model;
using SmartCharge.Api.Service.ChargeStation;
using Xunit;

namespace SmartCharge.Api.Tests.Service.ChargeStation
{
    public class UpdateChargeStation
    {
        public class UpdateChargeStationFixture
        {
            private readonly Mock<IMediator> _mockMediator;

            public UpdateChargeStationFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }

            public UpdateChargeStationFixture SetupMediator(int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.ChargeStation.UpdateChargeStation>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(affectedRows);
                return this;
            }

            public UpdateChargeStationFixture CheckUpdateChargeStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.ChargeStation.UpdateChargeStation>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }

            public UpdateChargeStationHandler CreateSut()
            {
                return new UpdateChargeStationHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectNotFound_WhenStationNotFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.UpdateChargeStation();
            var fixture = new UpdateChargeStationFixture();
            var updateChargeStationHandler = fixture
                .SetupMediator(0)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await updateChargeStationHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckUpdateChargeStation(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station is not found!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ExpectUpdated_WhenStationFound()
        {
            //Arrange
            var request = new Api.Service.ChargeStation.UpdateChargeStation();
            var fixture = new UpdateChargeStationFixture();
            var updateChargeStationHandler = fixture
                .SetupMediator(1)
                .CreateSut();

            //Act
            await updateChargeStationHandler.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckUpdateChargeStation(Times.Once());
        }
    }
}