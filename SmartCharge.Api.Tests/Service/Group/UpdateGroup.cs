using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Model;
using SmartCharge.Api.Service.Group;
using Xunit;

namespace SmartCharge.Api.Tests.Service.Group
{
    public class UpdateGroup
    {
        public class UpdateGroupFixture
        {
            private readonly Mock<IMediator> _mockMediator;
            
            public UpdateGroupFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }
            
            public UpdateGroupFixture SetupMediator(double maxCurrent, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Connector.GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(maxCurrent);
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Group.UpdateGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(affectedRows);
                return this;
            }
            
            public UpdateGroupFixture CheckGetMaxCurrentOfGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Connector.GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateGroupFixture CheckUpdateGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Group.UpdateGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateGroupHandler CreateSut()
            {
                return new UpdateGroupHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectBadRequest_WhenCapacityLesserThanMaxCurrent()
        {
            //Arrange
            var request = new Api.Service.Group.UpdateGroup{ Key = It.IsAny<Guid>(), Name = It.IsAny<string>(), Capacity = 10};
            var fixture = new UpdateGroupFixture();
            var updateGroupHandler = fixture
                .SetupMediator(11, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateGroupHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckUpdateGroup(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("Max current is larger than requested capacity!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectNotFound_WhenTheGroupDoesNotExist()
        {
            //Arrange
            var request = new Api.Service.Group.UpdateGroup{ Key = It.IsAny<Guid>(), Name = It.IsAny<string>(), Capacity = 10};
            var fixture = new UpdateGroupFixture();
            var updateGroupHandler = fixture
                .SetupMediator(9, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateGroupHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckUpdateGroup(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group is not found!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }
        
        [Fact]
        public async Task ExpectSuccessfullyUpdated_WhenCapacityIsHigherAndGroupExists()
        {
            //Arrange
            var request = new Api.Service.Group.UpdateGroup{ Key = It.IsAny<Guid>(), Name = It.IsAny<string>(), Capacity = 10};
            var fixture = new UpdateGroupFixture();
            var updateGroupFixture = fixture
                .SetupMediator(9, 1)
                .CreateSut();
            
            //Act
            await updateGroupFixture.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckUpdateGroup(Times.Once());
        }
    }
}