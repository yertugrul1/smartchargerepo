using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Model;
using SmartCharge.Api.Service.Group;
using Xunit;

namespace SmartCharge.Api.Tests.Service.Group
{
    public class AddGroup
    {
        public class AddGroupFixture
        {
            private readonly Mock<IMediator> _mockMediator;
            
            public AddGroupFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }
            
            public AddGroupFixture SetupMediator(int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Group.AddGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(affectedRows);
                return this;
            }
            
            public AddGroupFixture CheckAddGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Group.AddGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public AddGroupHandler CreateSut()
            {
                return new AddGroupHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectBadRequest_WhenTheGroupAlreadyExists()
        {
            //Arrange
            var request = new Api.Service.Group.AddGroup{ Key = It.IsAny<Guid>(), Name = It.IsAny<string>()};
            var fixture = new AddGroupFixture();
            var addGroupHandler = fixture
                .SetupMediator(0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await addGroupHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckAddGroup(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group already exists!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectSuccessfullyAdd_WhenTheGroupDoesNotExistBefore()
        {
            //Arrange
            var request = new Api.Service.Group.AddGroup{ Key = It.IsAny<Guid>(), Name = It.IsAny<string>()};
            var fixture = new AddGroupFixture();
            var addGroupHandler = fixture
                .SetupMediator(1)
                .CreateSut();
            
            //Act
            await addGroupHandler.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckAddGroup(Times.Once());
        }
    }
}