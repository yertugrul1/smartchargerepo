using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using Xunit;
using DeleteGroupHandler = SmartCharge.Api.Service.Group.DeleteGroupHandler;

namespace SmartCharge.Api.Tests.Service.Group
{
    public class DeleteGroup
    {
        public class DeleteGroupFixture
        {
            private readonly Mock<IMediator> _mockMediator;

            public DeleteGroupFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }

            public DeleteGroupFixture SetupMediator(GetGroupResponse group)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(group);
                return this;
            }

            public DeleteGroupFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }

            public DeleteGroupFixture CheckDeleteGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Group.DeleteGroup>(), It.IsAny<CancellationToken>()),
                    times);
                return this;
            }

            public DeleteGroupFixture CheckDeleteGroupEvent(Times times)
            {
                _mockMediator.Verify(o => o.Publish(It.IsAny<Notification.DeleteGroupEvent>(), It.IsAny<CancellationToken>()), times);
                return this;
            }

            public DeleteGroupHandler CreateSut()
            {
                return new DeleteGroupHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectNotFound_WhenGroupNotFound()
        {
            //Arrange
            var request = new Api.Service.Group.DeleteGroup(Guid.Empty);
            var fixture = new DeleteGroupFixture();
            var deleteGroupHandler = fixture
                .SetupMediator(null)
                .CreateSut();

            //Act
            var ex = await Record.ExceptionAsync(async () =>
                await deleteGroupHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckDeleteGroup(Times.Never());
            fixture.CheckDeleteGroupEvent(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group is not found!");
            ((CustomException) ex).StatusCode.Should().Be((int) HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ExpectDeleted_WhenGroupFound()
        {
            //Arrange
            var request = new Api.Service.Group.DeleteGroup(Guid.Empty);
            var fixture = new DeleteGroupFixture();
            var deleteGroupFixture = fixture
                .SetupMediator(new GetGroupResponse())
                .CreateSut();

            //Act
            await deleteGroupFixture.Handle(request, It.IsAny<CancellationToken>());

            //Assert
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckDeleteGroup(Times.Once());
            fixture.CheckDeleteGroupEvent(Times.Once());
        }
    }
}