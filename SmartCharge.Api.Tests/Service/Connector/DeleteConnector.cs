using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using SmartCharge.Api.Service.Connector;
using Xunit;

namespace SmartCharge.Api.Tests.Service.Connector
{
    public class DeleteConnector
    {
        public class DeleteConnectorFixture
        {
            private readonly Mock<IMediator> _mockMediator;
            
            public DeleteConnectorFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }
            
            public DeleteConnectorFixture SetupMediator(GetStationResponse station, GetGroupResponse group, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>())).ReturnsAsync(station);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(group);
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Connector.DeleteConnector>(), It.IsAny<CancellationToken>())).ReturnsAsync(affectedRows);
                
                return this;
            }
            
            public DeleteConnectorFixture CheckGetStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public DeleteConnectorFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public DeleteConnectorFixture CheckDeleteConnector(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Connector.DeleteConnector>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public DeleteConnectorHandler CreateSut()
            {
                return new DeleteConnectorHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotExists()
        {
            //Arrange
            var request = new Api.Service.Connector.DeleteConnector(It.IsAny<Guid>(), It.IsAny<int>());
            var fixture = new DeleteConnectorFixture();
            var deleteConnectorHandler = fixture
                .SetupMediator(null, null, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await deleteConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Never());
            fixture.CheckDeleteConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not exists!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotHaveGroup()
        {
            //Arrange
            var request = new Api.Service.Connector.DeleteConnector(It.IsAny<Guid>(), It.IsAny<int>());
            var fixture = new DeleteConnectorFixture();
            var deleteConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, null, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await deleteConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));
        
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckDeleteConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not have any group!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectNotFound_WhenTheConnectorNotFound()
        {
            //Arrange
            var request = new Api.Service.Connector.DeleteConnector(It.IsAny<Guid>(), It.IsAny<int>());
            var fixture = new DeleteConnectorFixture();
            var deleteConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await deleteConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));
        
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckDeleteConnector(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The connector is not found!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task ExpectDeleted_WhenTheRequestSuccessful()
        {
            //Arrange
            var request = new Api.Service.Connector.DeleteConnector(It.IsAny<Guid>(), It.IsAny<int>());
            var fixture = new DeleteConnectorFixture();
            var deleteConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, 1)
                .CreateSut();
            
            //Act
            await deleteConnectorHandler.Handle(request, It.IsAny<CancellationToken>());
            
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckDeleteConnector(Times.Once());
        }
    }
}