using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Connector;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using Xunit;
using UpdateConnectorHandler = SmartCharge.Api.Service.Connector.UpdateConnectorHandler;

namespace SmartCharge.Api.Tests.Service.Connector
{
    public class UpdateConnector
    {
        public class UpdateConnectorFixture
        {
            private readonly Mock<IMediator> _mockMediator;
            
            public UpdateConnectorFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }
            
            public UpdateConnectorFixture SetupMediator(GetStationResponse station, GetGroupResponse group, GetConnectorResponse connector, double maxCurrent, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>())).ReturnsAsync(station);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(group);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetConnector>(), It.IsAny<CancellationToken>())).ReturnsAsync(connector);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(maxCurrent);
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Connector.UpdateConnector>(), It.IsAny<CancellationToken>())).ReturnsAsync(affectedRows);
                
                return this;
            }
            
            public UpdateConnectorFixture CheckGetStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateConnectorFixture CheckGetConnector(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetConnector>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateConnectorFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateConnectorFixture CheckGetMaxCurrentOfGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateConnectorFixture CheckUpdateConnector(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Connector.UpdateConnector>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public UpdateConnectorHandler CreateSut()
            {
                return new UpdateConnectorHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotExists()
        {
            //Arrange
            var request = new Api.Service.Connector.UpdateConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                                                                 StationKey = It.IsAny<Guid>()};
            var fixture = new UpdateConnectorFixture();
            var updateConnectorHandler = fixture
                .SetupMediator(null, null, null, 0, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Never());
            fixture.CheckGetMaxCurrentOfGroup(Times.Never());
            fixture.CheckGetConnector(Times.Never());
            fixture.CheckUpdateConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not exists!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotHaveGroup()
        {
            //Arrange
            var request = new Api.Service.Connector.UpdateConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new UpdateConnectorFixture();
            var updateConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, null, null, 0, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Never());
            fixture.CheckGetConnector(Times.Never());
            fixture.CheckUpdateConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not have any group!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectNotFound_WhenTheConnectorNotExists()
        {
            //Arrange
            var request = new Api.Service.Connector.UpdateConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new UpdateConnectorFixture();
            var updateConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, null, 0, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Never());
            fixture.CheckGetConnector(Times.Once());
            fixture.CheckUpdateConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The connector is not found!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }
        
        [Theory]
        [InlineData(10, 5, 2, 8)]
        [InlineData(10, 8, 5, 8)]
        [InlineData(10, 7, 2, 6)]
        public async Task ExpectBadRequest_WhenTheCapacityExceeds(double capacity, double totalCurrent, double existingConnectorCurrent, double updatedConnectorCurrent)
        {
            //Arrange
            var request = new Api.Service.Connector.UpdateConnector{Id = It.IsAny<int>(), MaxCurrent = updatedConnectorCurrent, 
                StationKey = It.IsAny<Guid>()};
            var fixture = new UpdateConnectorFixture();
            var updateConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = capacity, Key = Guid.NewGuid().ToString() }, 
                            new GetConnectorResponse{ MaxCurrent = existingConnectorCurrent}, totalCurrent, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await updateConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));
        
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckGetConnector(Times.Once());
            fixture.CheckUpdateConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group capacity is exceeding!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ExpectUpdated_WhenTheRequestSuccessfulAndCapacityNotExceeds()
        {
            //Arrange
            var request = new Api.Service.Connector.UpdateConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new UpdateConnectorFixture();
            var updateConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, 
                      new GetConnectorResponse(), It.IsAny<double>(), 1)
                .CreateSut();
            
            //Act
            await updateConnectorHandler.Handle(request, It.IsAny<CancellationToken>());
            
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckGetConnector(Times.Once());
            fixture.CheckUpdateConnector(Times.Once());
        }
    }
}