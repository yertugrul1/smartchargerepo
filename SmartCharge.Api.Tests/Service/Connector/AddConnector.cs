using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Moq;
using SmartCharge.Api.Data.ChargeStation;
using SmartCharge.Api.Data.Connector;
using SmartCharge.Api.Data.Group;
using SmartCharge.Api.Model;
using Xunit;
using AddConnectorHandler = SmartCharge.Api.Service.Connector.AddConnectorHandler;

namespace SmartCharge.Api.Tests.Service.Connector
{
    public class AddConnector
    {
        public class AddConnectorFixture
        {
            private readonly Mock<IMediator> _mockMediator;
            
            public AddConnectorFixture()
            {
                _mockMediator = new Mock<IMediator>();
            }
            
            public AddConnectorFixture SetupMediator(GetStationResponse station, GetGroupResponse group, double maxCurrent, int affectedRows)
            {
                _mockMediator.Setup(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>())).ReturnsAsync(station);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(group);
                _mockMediator.Setup(o => o.Send(It.IsAny<GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>())).ReturnsAsync(maxCurrent);
                _mockMediator.Setup(o => o.Send(It.IsAny<Data.Connector.AddConnector>(), It.IsAny<CancellationToken>())).ReturnsAsync(affectedRows);
                
                return this;
            }
            
            public AddConnectorFixture CheckGetStation(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetStation>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public AddConnectorFixture CheckGetGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public AddConnectorFixture CheckGetMaxCurrentOfGroup(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<GetMaxCurrentOfGroup>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public AddConnectorFixture CheckAddConnector(Times times)
            {
                _mockMediator.Verify(o => o.Send(It.IsAny<Data.Connector.AddConnector>(), It.IsAny<CancellationToken>()), times);
                return this;
            }
            
            public AddConnectorHandler CreateSut()
            {
                return new AddConnectorHandler(_mockMediator.Object);
            }
        }

        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotExists()
        {
            //Arrange
            var request = new Api.Service.Connector.AddConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                                                                 StationKey = It.IsAny<Guid>()};
            var fixture = new AddConnectorFixture();
            var addConnectorHandler = fixture
                .SetupMediator(null, null, 0, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await addConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Never());
            fixture.CheckGetMaxCurrentOfGroup(Times.Never());
            fixture.CheckAddConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not exists!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenTheStationNotHaveGroup()
        {
            //Arrange
            var request = new Api.Service.Connector.AddConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new AddConnectorFixture();
            var addConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, null, 0, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await addConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Never());
            fixture.CheckAddConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The charge station does not have any group!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Theory]
        [InlineData(10, 9, 2)]
        [InlineData(10, 8, 3)]
        [InlineData(10, 3, 8)]
        public async Task ExpectBadRequest_WhenTheCapacityExceeds(double capacity, double totalCurrent, double newConnectorCurrent)
        {
            //Arrange
            var request = new Api.Service.Connector.AddConnector{Id = It.IsAny<int>(), MaxCurrent = newConnectorCurrent, 
                StationKey = It.IsAny<Guid>()};
            var fixture = new AddConnectorFixture();
            var addConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = capacity, Key = Guid.NewGuid().ToString() }, 
                    totalCurrent, 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await addConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckAddConnector(Times.Never());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The group capacity is exceeding!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectBadRequest_WhenTheConnectorAlreadyExists()
        {
            //Arrange
            var request = new Api.Service.Connector.AddConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new AddConnectorFixture();
            var addConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, 
                    It.IsAny<double>(), 0)
                .CreateSut();
            
            //Act
            var ex = await Record.ExceptionAsync(async () => await addConnectorHandler.Handle(request, It.IsAny<CancellationToken>()));

            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckAddConnector(Times.Once());
            ex.Should().BeOfType<CustomException>();
            ex.Message.Should().Be("The connector already exists!");
            ((CustomException)ex).StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public async Task ExpectAdded_WhenTheRequestSuccessfulAndCapacityNotExceeds()
        {
            //Arrange
            var request = new Api.Service.Connector.AddConnector{Id = It.IsAny<int>(), MaxCurrent = It.IsAny<double>(), 
                StationKey = It.IsAny<Guid>()};
            var fixture = new AddConnectorFixture();
            var addConnectorHandler = fixture
                .SetupMediator(new GetStationResponse{GroupKey = Guid.NewGuid().ToString()}, new GetGroupResponse{Capacity = It.IsAny<double>(), Key = Guid.NewGuid().ToString() }, 
                    It.IsAny<double>(), 1)
                .CreateSut();
            
            //Act
            await addConnectorHandler.Handle(request, It.IsAny<CancellationToken>());
            
            //Assert
            fixture.CheckGetStation(Times.Once());
            fixture.CheckGetGroup(Times.Once());
            fixture.CheckGetMaxCurrentOfGroup(Times.Once());
            fixture.CheckAddConnector(Times.Once());
        }
    }
}